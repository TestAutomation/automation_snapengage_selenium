package de.snapengage.automation.pages;

import org.testng.Assert;

public class HubPage extends BasePage {

    public String pageUrl = "/hub";
    private String pageTitle = "Hub";

    public void checkIsOpened() {
        String expectedUrl = super.baseUrl + pageUrl;
        Assert.assertTrue(driver.getCurrentUrl().contentEquals(expectedUrl));
        Assert.assertTrue(driver.getTitle().contentEquals(pageTitle));
    }
}
