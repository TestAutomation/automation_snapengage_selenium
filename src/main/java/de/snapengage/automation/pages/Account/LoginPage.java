package de.snapengage.automation.pages.Account;

import de.snapengage.automation.pages.BasePage;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    private String pageUrl = "/signin?to=hub";
    private String emailInput = "#email";
    private String passwordInput = "#password";
    private String submitLogin = "[name='Submit']";

    public void open(){
        driver.get(super.baseUrl + pageUrl);
    }

    public void login(String email, String password){
        driver.findElement(By.cssSelector(emailInput)).sendKeys(email);
        driver.findElement(By.cssSelector(passwordInput)).sendKeys(password);
        driver.findElement(By.cssSelector(submitLogin)).click();
    }
}
