package de.snapengage.automation.pages;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BasePage {

    protected static WebDriver driver;
    public String baseUrl = "https://snapengage-qa.appspot.com";

    public void setWebDriver(WebDriver driver) {
        BasePage.driver = driver;
    }

    public void takeScreenshot() throws IOException {
        String pageName = getClass().getSimpleName();
        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("src/screenshots/" + pageName + "_" + new SimpleDateFormat("DD-MM-YYYY").format(new Date()) + ".png"));

    }
}
