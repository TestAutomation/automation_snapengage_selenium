package de.snapengage.automation.tests.Account;

import de.snapengage.automation.pages.Account.LoginPage;
import de.snapengage.automation.pages.HubPage;
import de.snapengage.automation.tests.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @DataProvider(name = "canLoginCases", parallel = true)
    public Object[][] canLoginCases() {
        return new Object[][]{
                {
                        "pedroalmodovar@test.com",
                        "1q2w3e"
                },
        };
    }

    @Test(dataProvider = "canLoginCases")
    public void registeredUserCanLogin(String email,
                                       String password) throws Exception {
        LoginPage loginPage = new LoginPage();
        loginPage.open();

        loginPage.login(email, password);

        HubPage hubPage = new HubPage();
        hubPage.checkIsOpened();
        loginPage.takeScreenshot();
    }
}
