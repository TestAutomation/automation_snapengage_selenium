package de.snapengage.automation.tests;

import com.codeborne.selenide.testng.ScreenShooter;
import de.snapengage.automation.pages.BasePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({ScreenShooter.class})
public class BaseTest {

    public static WebDriver driver;
    protected static BasePage basePage;

    @BeforeMethod
    public void setUp() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        basePage = new BasePage();
        basePage.setWebDriver(driver);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
